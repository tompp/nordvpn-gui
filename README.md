# README - NordVPNGUI

GUI to check NordVPN servers status, connect to them & etc.
### Screenshot
![scrot](scrot.jpeg 
"Screenshot")

### Dependencies
######Required
* openvpn
* update-resolv-conf (https://github.com/masterkorp/openvpn-update-resolv-conf)
* numpy
* PyQt5
* speedtest_cli

### FEATURES

* List all the available servers and their distance from current location
* List all loads of the servers
* Speedtest the connection within the UI (currently disabled, to be enabled in future)

### TODO:

* Create installer
* Fix all the TODO's from code
* and the list continues on file...
	
### SETUP & USAGE

1. Install dependencies
1. Clone the repo.
1. Modify the openvpn location in pingnordvpn_mainv2.py if not found in /etc/openvpn.
1. Modify the location of update-resolv-conf, if not found in /etc/openvpn.
1. openvpn needs sudo, so either add pingnordvpn_mainv2.py to sudoers (not recommended) or type password 
when connecting


Usage:

>	python3.4 /path/to/file

or

>	python /path/to/file

depending on the python3 path
