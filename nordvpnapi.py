# -*- coding: utf-8 -*-

import codecs
import re
import urllib
import json
import pprint as pp
from math import radians, cos, sin, asin, sqrt
import requests

class nordvpnAPI():
    """class for getting data from NordVPN API"""

    def __init__(self):
        #self.dataServerLoad = self.getServerData()
        #self.dataServer, self.dataLoad = self.formatServerLoad(self.dataServerLoad) #DEPRECATED
        self.reader = codecs.getreader("utf-8")
        self.lat1, self.lon1, self.ipAddress, self.countryCode = self.getLocation()
        self.town, self.country = self.getCity(self.lat1, self.lon1)
        self.dataServerRaw = self.getRawData()
        # This dictionary will be filled with domain : ip pairs in formatServerSpecs method
        self.serverIPs = {}
        self.dataServerSpecs = self.formatServerSpecs(self.dataServerRaw)
        self.dataServer, self.dataLoad, self.dataLatLong, self.dataMode, self.dataAddress = self.serverSpecs2Tables(self.dataServerSpecs)

    def refreshInfo(self):
        self.lat1, self.lon1, self.ipAddress, self.countryCode = self.getLocation()
        self.town, self.country = self.getCity(self.lat1, self.lon1)

    def refreshServers(self):
        self.dataServerRaw = self.getRawData()
        self.dataServerSpecs = self.formatServerSpecs(self.dataServerRaw)
        self.dataServer, self.dataLoad, self.dataLatLong, self.dataMode, self.dataAddress = self.serverSpecs2Tables(self.dataServerSpecs)

    def getLocation(self):
        rawLocation = json.load(self.reader(urllib.request.urlopen("http://ipinfo.io/json")))
        countryCode = rawLocation["country"]
        ipAddr = rawLocation["ip"]
        latlong = re.findall("loc\': \'(.*?)\'", str(rawLocation))
        latlong = latlong[0].replace("'","").split(",")
        return latlong[0], latlong[1], ipAddr, countryCode

# Traceback (most recent call last):
#   File "./pingnordvpn_mainv2.py", line 206, in refreshInfo
#     self.tester.nord.refreshInfo()
#   File "/home/elementary/sync/oma/pytho/pingnordvpnQt/nordvpn-gui/nordvpnapi.py", line 28, in refreshInfo
#     self.town, self.country = self.getCity(self.lat1, self.lon1)
#   File "/home/elementary/sync/oma/pytho/pingnordvpnQt/nordvpn-gui/nordvpnapi.py", line 48, in getCity
#     components = j['results'][0]['address_components']
# IndexError: list index out of range
#  File "/home/elementary/sync/oma/pytho/pingnordvpnQt/nordvpn-gui/nordvpnapi.py", line 57, in getCity
#    components = j['results'][0]['address_components']
#IndexError: list index out of range

    def getCity(self, lat, lon):
        url = "http://maps.googleapis.com/maps/api/geocode/json?"
        url += "latlng=%s,%s&sensor=false" % (lat, lon)
        v = self.reader(urllib.request.urlopen(url))
        j = json.load(v)
        try:
            components = j['results'][0]['address_components']
        except IndexError:
            if j['status'] == 'OVER_QUERY_LIMIT':
                return "ERROR", "ERROR"
            return "ERROR2", "ERROR2"
        country = town = None
        for c in components:
            #print(c)
            if "country" in c['types']:
                country = c['long_name']
            if "postal_town" in c['types']:
                town = c['long_name']
            if town is None:
                tryLoop2 = True
        if tryLoop2:
            town = []
            for c in components:
                index = 3
                adminArea = "administrative_area_level_%s" % (index)
                while index > 0:
                    adminArea = "administrative_area_level_%s" % (index)
                    if adminArea in c['types']:
                        town.append(c['long_name'])
                    index -= 1
        return town, country

    def getServerData(self):
        response = requests.get(
            url='https://api.nordvpn.com/server/stats',
            headers={
                'User-Agent': 'NordVPN_Client_5.56.780.0',
                'Host': 'api.nordvpn.com',
                'Connection': 'Close'
            }
        )
        return response.text

    def formatServerLoad(self, data):
        dataServer = re.findall('\"(.*?)\"', data)
        dataServerFormatted = []
        for item in dataServer:
            if item != "percent":
                dataServerFormatted.append(item)
        dataLoad = re.findall('t\":(.*?)}', data)
        return dataServerFormatted, dataLoad

    def formatServerSpecs(self, data):
        splittedList = data.split("}},{")
        data2 = json.loads(data)
        index = 0
        parsedlist = []
        while index < len(data2):
            parsedlist.append([])
            parsedlist[index].append(data2[index]['name'])
            parsedlist[index].append(data2[index]['domain'])
            self.serverIPs[data2[index]['domain']] = data2[index]['ip_address']
            parsedlist[index].append(data2[index]['features']['openvpn_tcp'])
            parsedlist[index].append(data2[index]['features']['openvpn_udp'])
            parsedlist[index].append(data2[index]['location']['long'])
            parsedlist[index].append(data2[index]['location']['lat'])
            parsedlist[index].append(data2[index]['load'])
            try:
                parsedlist[index].append(data2[index]['categories'][0]['name'])
            except IndexError as indxError:
                pass
            try:
                parsedlist[index].append(data2[index]['categories'][1]['name'])
            except IndexError as indxError:
                pass
            parsedlist[index].append(data2[index]['search_keywords'])
            index += 1
        return parsedlist
        # for item in splittedList:
        #     pp.pprint(item)
### EXAMPLE OUTPUT OF UPPER
# "id":590551,"ip_address":"109.236.84.4","search_keywords":[],"categories":[{"name":"Standard VPN servers"},{"name":"P2P"}],"name":"Netherlands #14","domain":"nl14.nordvpn.com","price":0,"flag":"NL","country":"Netherlands","location":{"lat":52.3667,"long":4.9},"load":75,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590552,"ip_address":"104.250.122.26","search_keywords":["Netflix","ABC (US)","Hulu"],"categories":[{"name":"Standard VPN servers"}],"name":"United States #318","domain":"us318.nordvpn.com","price":0,"flag":"US","country":"United States","location":{"lat":34.05223,"long":-118.24368},"load":59,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590553,"ip_address":"104.250.122.34","search_keywords":["Netflix","ABC (US)","Hulu"],"categories":[{"name":"Standard VPN servers"}],"name":"United States #319","domain":"us319.nordvpn.com","price":0,"flag":"US","country":"United States","location":{"lat":34.05223,"long":-118.24368},"load":57,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590554,"ip_address":"104.250.122.42","search_keywords":["Netflix","ABC (US)","Hulu"],"categories":[{"name":"Anti DDoS"}],"name":"United States #320","domain":"us320.nordvpn.com","price":0,"flag":"US","country":"United States","location":{"lat":34.05223,"long":-118.24368},"load":80,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590555,"ip_address":"104.145.229.2","search_keywords":["Netflix","ABC (US)","Hulu"],"categories":[{"name":"Ultra fast TV"}],"name":"United States #321","domain":"us321.nordvpn.com","price":0,"flag":"US","country":"United States","location":{"lat":25.7824618,"long":-80.30112},"load":89,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590556,"ip_address":"104.145.229.10","search_keywords":["Netflix","ABC (US)","Hulu"],"categories":[{"name":"Standard VPN servers"}],"name":"United States #322","domain":"us322.nordvpn.com","price":0,"flag":"US","country":"United States","location":{"lat":25.7824618,"long":-80.30112},"load":76,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590557,"ip_address":"103.15.187.52","search_keywords":["Nippon TV"],"categories":[{"name":"Standard VPN servers"}],"name":"Japan #5","domain":"jp5.nordvpn.com","price":0,"flag":"JP","country":"Japan","location":{"lat":35.780602,"long":139.172022},"load":54,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590558,"ip_address":"92.39.242.3","search_keywords":["Rai TV"],"categories":[{"name":"Standard VPN servers"},{"name":"P2P"}],"name":"Italy #5","domain":"it5.nordvpn.com","price":0,"flag":"IT","country":"Italy","location":{"lat":45.1611,"long":10.8},"load":69,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590559,"ip_address":"67.215.4.90","search_keywords":["Skype","Double VPN"],"categories":[{"name":"Double VPN"}],"name":"United States - Canada #2","domain":"us-ca2.nordvpn.com","price":0,"flag":"CA","country":"Canada","location":{"lat":45.504,"long":-73.5747},"load":73,"features":{"openvpn_tcp":true,"openvpn_udp":false,"ikev2":false
# "id":590560,"ip_address":"41.77.136.74","search_keywords":[],"categories":[{"name":"Standard VPN servers"}],"name":"Egypt #1","domain":"eg1.nordvpn.com","price":0,"flag":"EG","country":"Egypt","location":{"lat":30.0771,"long":31.2859},"load":53,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590561,"ip_address":"95.141.37.164","search_keywords":["Rai TV"],"categories":[{"name":"Anti DDoS"}],"name":"Italy #6","domain":"it6.nordvpn.com","price":0,"flag":"IT","country":"Italy","location":{"lat":45.1611,"long":10.8},"load":74,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590562,"ip_address":"95.141.37.229","search_keywords":["Rai TV"],"categories":[{"name":"Standard VPN servers"}],"name":"Italy #7","domain":"it7.nordvpn.com","price":0,"flag":"IT","country":"Italy","location":{"lat":45.1611,"long":10.8},"load":68,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590564,"ip_address":"94.242.255.143","search_keywords":["P2P","Torrents","Vuze","uTorrent","BitTorrent","Download"],"categories":[{"name":"Standard VPN servers"},{"name":"P2P"}],"name":"Luxembourg #3","domain":"lu3.nordvpn.com","price":0,"flag":"LU","country":"Luxembourg","location":{"lat":49.75,"long":6.1667},"load":72,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590565,"ip_address":"94.242.255.159","search_keywords":["P2P","Torrents","Vuze","uTorrent","BitTorrent","Download","Skype"],"categories":[{"name":"Standard VPN servers"},{"name":"P2P"}],"name":"Luxembourg #4","domain":"lu4.nordvpn.com","price":0,"flag":"LU","country":"Luxembourg","location":{"lat":49.75,"long":6.1667},"load":72,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true
# "id":590568,"ip_address":"125.212.220.154","search_keywords":[],"categories":[{"name":"Standard VPN servers"}],"name":"Vietnam #1","domain":"vn1.nordvpn.com","price":0,"flag":"VN","country":"Vietnam","location":{"lat":21.0333,"long":105.85},"load":47,"features":{"openvpn_tcp":true,"openvpn_udp":true,"ikev2":true

    def displayIP(self):
        response = requests.get(
            url = 'https://api.nordvpn.com/user/address',
            headers = {
                'User-Agent': 'NordVPN_Client_5.56.780.0',
                'Host': 'api.nordvpn.com',
                'Connection':'Close'
            }
        )
        return response.text

    def getRawData(self):
        response = requests.get(
            url = 'https://api.nordvpn.com/server',
            headers = {
                'User-Agent': 'NordVPN_Client_5.56.780.0',
                'Host': 'api.nordvpn.com',
                'Connection':'Close'
            }
        )
        return response.text

    def haversine(self, lat1, lon1, lat2, lon2):
        """
        Calculate the great circle distance between two points
        on the earth (specified in decimal degrees)
        """
        # convert decimal degrees to radians
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
        c = 2 * asin(sqrt(a))
        km = 6367 * c
        return km

    def serverSpecs2Tables(self, serverSpecs):
        dataServer = []
        dataLoad = []
        dataLatLong = []
        dataMode = []
        dataAddress = []
        index = 0
        while index < len(serverSpecs):
            dataServer.append(serverSpecs[index][0])
            index += 1
        index = 0
        while index < len(serverSpecs):
            dataLoad.append(float(serverSpecs[index][6]))
            index += 1
        index = 0
        while index < len(serverSpecs):
            dataLatLong.append(
                self.haversine(
                    float(serverSpecs[index][5]),
                    float(serverSpecs[index][4]),
                    float(self.lat1),
                    float(self.lon1)
                    ))
            index += 1
        index = 0
        while index < len(serverSpecs):
            specs1 = serverSpecs[index][7]
            ## TODO: FIX THIS DIRTY FIX
            specs1 = str(specs1)
            try:
                specs2 = serverSpecs[index][8]
            except Exception as ex:
                pass
            if isinstance(specs2, str):
                dataMode.append(specs1 + " (" + specs2 + ")")
            elif specs2 != [] and isinstance(specs2, list):
                dataMode.append(specs1 + " (" + ", ".join(specs2) + ")")
            else:
                dataMode.append(serverSpecs[index][7])
            index += 1
        index = 0
        while index < len(serverSpecs):
            dataAddress.append(serverSpecs[index][1])
            index += 1
        return dataServer, dataLoad, dataLatLong, dataMode, dataAddress


    def filterServers(self, checkboxes, distance):
        toBeKept = []
        if checkboxes != []:
            for idx in range(len(checkboxes)):
                if checkboxes[idx][1]:
                    for i in range(len(self.dataMode)):
                        if any(x in self.dataMode[i] for x in checkboxes[idx][0].split()):
                            toBeKept.append(i)
        toBeKept = list(set(toBeKept))
        if toBeKept == [] and distance == "":
            return
        #print("toBeKept no duplicates:", toBeKept)
        toBeKept2 = []
        #print("distance:", distance, "its type:", type(distance), distance == "")
        if distance != "":
            if toBeKept == []:
                for i in range(len(self.dataLatLong)):
                    if self.dataLatLong[i] < float(distance):
                        toBeKept2.append(i)
                #print("toBeKetp2 in empty toBeKept:", toBeKept2)
            else:
                substract = []
                for i in range(len(toBeKept)):
                    if self.dataLatLong[toBeKept[i]] > float(distance):
                        substract.append(toBeKept[i])
                #print("substract:", substract)
                toBeKept2 = [x for x in toBeKept if x not in substract]
        else:
            toBeKept2 = toBeKept
        # print("tobeKept2:", toBeKept2)
        toBeRemoved = [x for x in range(0, len(self.dataMode)) if x not in toBeKept2]
        toBeRemoved = sorted(toBeRemoved, key=int, reverse=True)
        for i in range(len(toBeRemoved)):
            del self.dataServer[toBeRemoved[i]]
            del self.dataLoad[toBeRemoved[i]]
            del self.dataLatLong[toBeRemoved[i]]
            del self.dataMode[toBeRemoved[i]]
            del self.dataAddress[toBeRemoved[i]]
        ## Debugging purposes
        # for i in range(len(self.dataServer)):
        #     print(self.dataServer[i], self.dataLoad[i], self.dataLatLong[i], self.dataMode)
            
