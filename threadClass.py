#!/usr/bin/python3.4
# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets
from nordvpnapi import nordvpnAPI
import subprocess
import socket
import time
import speedtest
import requests
import zipfile
import io
import os
import fileinput
import sys

class CommunicatorClass(QtCore.QThread):
    # Create the signal
    text = QtCore.pyqtSignal(str)

    def __init__(self, GUI, parent=None):
        super(CommunicatorClass, self).__init__(parent)
        self.text.connect(GUI.textTest)
        self.message = "No message set"

    def run(self):
        self.text.emit(self.message)

    def setMessage(self, text):
        self.message = text

class ThreadClass(QtCore.QThread):
    
    # Create signal to pass server data
    getData = QtCore.pyqtSignal(int, list)

    # Create signal to pass info data
    getInfoData = QtCore.pyqtSignal(int, list)

    def __init__(self, GUI, communicator=None, parent=None):
        super(ThreadClass, self).__init__(parent)

        # Connect signal to the desired function
        #self.sig.connect(GUI.testProgress)

        # Set the state
        #self.serverInfo = serverInfo
        self.GUI = GUI
        self.nord = nordvpnAPI()
        self.communicator = communicator
        self.mode = 1
        self.basic = True
        self.checkboxes = []
        self.distance = ''

    def run(self):
        if self.mode == 1:
            #self.GUI.addTableServers(0, self.nord.dataServer)
            #self.GUI.addTableServers(1, self.nord.dataLoad)
            #self.GUI.addTableServers(2, self.nord.dataLatLong)
            #self.GUI.addTableServers(3, self.nord.dataMode)
            self.sendData(0, self.nord.dataServer)
            self.sendData(1, self.nord.dataLoad)
            self.sendData(2, self.nord.dataLatLong)
            self.sendData(3, self.nord.dataMode)
            # tableInfo
            #self.GUI.addTableInfo(1, [self.nord.ipAddress, self.nord.country, self.nord.town, time.strftime("%X")])
            if self.basic:
                self.sendInfoData(1, [self.nord.ipAddress, self.nord.country, self.nord.town, time.strftime("%X")])
                self.communicator.setMessage("Data sent\n")
                self.communicator.start()
            else:
                self.communicator.setMessage("Minimum done!\n")
                self.communicator.start()
        elif self.mode == 2:
            self.nord.refreshServers()
            if self.checkboxes != [] or self.distance != "":
                self.nord.filterServers(self.checkboxes, self.distance)

    def sendData(self, column, array):
        self.getData.emit(column, array)

    def sendInfoData(self, column, array):
        self.getInfoData.emit(column, array)

    def increaseProgress(self):
        val = 0
        while val < 100:
            val += 1
            # Emit the signal
            self.sig.emit(val)
            self.sleep(1)

    def connectVPN(self, serverInfo):
        #print(["Threads serverInfo", self.serverInfo])
        #(stdoutConnect, stderrConnect) = Popen(["sudo", "openvpn","--config", serverAddress, "--script-security", "2", "--up", updateResolv, "--down", updateResolv], stdout=PIPE, stderr=PIPE).communicate()
        serverAddress = "/etc/openvpn/%s.udp1194.ovpn" % serverInfo
        updateResolv = "/etc/openvpn/update-resolv-conf"
        p = subprocess.Popen(["sudo", "openvpn", "--config", serverAddress, "--script-security", "2", "--up", updateResolv, "--down", updateResolv], stdin=subprocess.PIPE, stdout=subprocess.PIPE, bufsize=1)
        for line in iter(p.stdout.readline, b''):
            print(line)
        p.stdout.close()
        p.wait()

    def setMode(self, mode, basic = True, checkboxes = [], distance = ''):
        self.mode = mode
        self.basic = basic
        self.checkboxes = checkboxes
        self.distance = distance

class RefreshThreadClass(QtCore.QThread):

    # Signal of the Refreshbutton
    sigref = QtCore.pyqtSignal(object, bool)

    def __init__(self, parent=None):
        super(RefreshThreadClass, self).__init__(parent)
        self.nord = nordvpnAPI()

    def run(self):
        #self.GUI.progressBar.setFormat("RefreshThreadClass start run")
        self.nord.refreshInfo()
        self.demandRefreshInfo()

    def demandRefreshInfo(self):
        self.sigref.emit(self.nord, self.nord.ipAddress in self.nord.serverIPs.values())

class ConnectionTestThreadClass(QtCore.QThread):

    # Signal of the OFFLINE
    offline = QtCore.pyqtSignal(bool)

    def __init__(self, parent=None):
        super(ConnectionTestThreadClass, self).__init__(parent)
        self.loop = True

    def run(self):
        print("connectionTester running")
        """
        Host: 8.8.8.8 (google-public-dns-a.google.com)
        OpenPort: 53/tcp
        Service: domain (DNS/TCP)
        """
        host="8.8.8.8"
        port=53
        timeout=3
        try:
            socket.setdefaulttimeout(timeout)
            socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
            self.sendConnectionTestResult(True)
        except Exception as ex:
            print(repr(ex))
            self.sendConnectionTestResult(False)

    def sendConnectionTestResult(self, result):
        self.offline.emit(result)

class SpeedtestThreadClass(QtCore.QThread):

    # Signal to pass the result
    result = QtCore.pyqtSignal(str, str)

    def __init__(self, parent=None):
        super(SpeedtestThreadClass, self).__init__(parent)
        self.servers = []

    def run(self):
        s = speedtest.Speedtest()
        s.get_servers(self.servers)
        s.get_best_server()
        downspeed = s.download() / 1000
        self.sendSpeedtestResult(downspeed, "Download")
        upspeed = s.upload() / 1000
        self.sendSpeedtestResult(upspeed, "Upload")
        #results_dict = s.results.dict()

    def sendSpeedtestResult(self, result, updown):
        result = '{:,}'.format(int("%5.f" % result)).replace(',', ' ')
        self.result.emit(result, updown)

class ConfiguratorThreadClass(QtCore.QThread):

    # Signal for the progress
    progress = QtCore.pyqtSignal()

    def __init__(self, parent=None, mode=0):
        super(ConfiguratorThreadClass, self).__init__(parent)
        self.mode = self.setMode(mode, init=True)
        self.settings = QtCore.QSettings()
        self.zipurl = "https://nordvpn.com/api/files/zip"
        self.settings.beginGroup("dialog/lines")
        self.location = self.settings.value("confpath", type=str)
        self.username = self.settings.value("username", type=str)
        self.password = self.settings.value("password", type=str)
        self.settings.endGroup()

    def run(self):
        self.downloadUnzipConfs()
        if self.mode == 1:
            self.modifyConfs()
        self.moveConfs()
        self.sendProgressInfo()

    def downloadUnzipConfs(self):
        request = requests.get(self.zipurl)
        confzip = zipfile.ZipFile(io.BytesIO(request.content))
        confzip.extractall(path=self.location)

    #TODO when first time running and put login info it doesnt save them into auth.txt file
    # instead of creating file auth.txt AND writing login info into file it only created the file
    # only after creating the file it was able to write login info
    def modifyConfs(self):
        print(os.path.join(self.location, "auth.txt"))
        with open(os.path.join(self.location, "auth.txt"), "w") as authfile:
            authfile.write(self.username + "\n" + self.password)
        for file in os.listdir(self.location):
            if file.endswith(".ovpn"):
                replacewith = "auth-user-pass " + self.location + "auth.txt"
                for line in fileinput.input(os.path.join(self.location, file), inplace=True):
                    line = line.replace("auth-user-pass", replacewith)
                    # sys.stdout is redirected to the file
                    sys.stdout.write(line)

    def moveConfs(self):
        pass
        # shutil.copytree(src, dst, symlinks=False, ignore=None, copy_function=copy2, ignore_dangling_symlinks=False)

    def sendProgressInfo(self):
        self.progress.emit()

    def setMode(self, mode, init=False):
        if 0 <= mode <= 1 and init:
            return mode
        elif 0 <= mode <= 1 and not init:
            self.mode = mode
        else:
            raise ValueError("Mode doesn't exist")

